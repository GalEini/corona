﻿using CoronaTrackerBL.Manager;
using CoronaTrackerBL.Others;
using CoronaTrackerDAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProjectCorona
{
    [TestClass]
    public class InfectedMangerTests
    {
        readonly InfectedManager infectedManger = InfectedManager.GetInstance();

        [TestMethod]
        public async Task CheckAddToHospital()
        {

            infectedManger.HospitalizePatient(6);
            infectedManger.HospitalizePatient(7);
            infectedManger.HospitalizePatient(5);
            await Task.Delay(3000);
            Assert.IsTrue(infectedManger.AllInfectedGroupByDays.ContainsKey(1));
            Assert.AreEqual(3, infectedManger.AllInfectedGroupByDays.GetOrAdd(1, 0));
            await Task.Delay(7000);
        }

        [TestMethod]
        public async Task CheckDischargeFromHospital()
        {

            var tempCount = infectedManger.AllInfectedGroupByDays.GetOrAdd(1, 0);
            infectedManger.HospitalizePatient(3);
            await Task.Delay(3000);
            Assert.AreEqual(tempCount + 1, infectedManger.AllInfectedGroupByDays.GetOrAdd(1, 0));
            infectedManger.DischargePatient(3);
            await Task.Delay(3000);
            Assert.AreEqual(tempCount, infectedManger.AllInfectedGroupByDays.GetOrAdd(1, 0));
            infectedManger.HospitalizePatient(3);
            await Task.Delay(3000);
            
        }
        [TestMethod]

        public async Task CheckincrementDays()
        {
            infectedManger.HospitalizePatient(2);
            var tempCount = infectedManger.AllInfectedGroupByDays.GetOrAdd(1, 0);
            infectedManger.IncreaseDaysInmemorey();
            await Task.Delay(3000);
            Assert.IsTrue(infectedManger.AllInfectedGroupByDays.ContainsKey(2));
            Assert.AreEqual(tempCount, infectedManger.AllInfectedGroupByDays.GetOrAdd(2, 0));
            await Task.Delay(7000);

        }
    }
        
}
