﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CoronaTrackerBL.LogisticsManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProjectCorona
{
    [TestClass]
    public class LogisticsManagerTest
    {
        readonly ILogisticsManager manager = LogisticsManager.GetInstance();
        [TestMethod]
        public void AmountSmallerThanStock()
        {
            //Assined at construction manager.kitsAmount =  1500;

            Assert.AreEqual(100, manager.WithdrawTestKits(100));
        }
        [TestMethod]
        public void AmountBiggerThanStock()
        {
            //Assined at construction manager.kitsAmount =  1500;

            Assert.AreNotEqual(2000, manager.WithdrawTestKits(2000));
        }
        public async Task OrderKitsAfterStockBellowMin()
        {
            bool first = manager.HasOrdered();
            manager.WithdrawTestKits(1001);
            await Task.Delay(2000);
            bool second = manager.HasOrdered();
            Assert.AreNotEqual(first, second);
        }
        [TestMethod]
        public void OrderKitsWhileNotOrdered()
        {
            bool first = manager.HasOrdered();
            manager.OrderTestKitsToStock(50);
            bool second = manager.HasOrdered();
            Assert.AreNotEqual(first, second);
        }
        [TestMethod]
        public void OrderKitsWhileAlreadyOrdered()
        {
            manager.OrderTestKitsToStock(50);
            bool first = manager.HasOrdered();
            manager.OrderTestKitsToStock(50);
            bool second = manager.HasOrdered();
            Assert.AreEqual(first, second);
        }
        

    }
}
