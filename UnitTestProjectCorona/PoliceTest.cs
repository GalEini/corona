﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using CoronaTrackerBL.Police;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProjectCorona
{
    [TestClass]
    public class PoliceTest
    {
        private static readonly string conStr =
             @"Data Source=DESKTOP-GAL-EIN;Initial Catalog=CoronaTrackerYaelGal;"
             + "Integrated Security=true";
        [TestMethod]
        public void CriminalRecordCheck()
        {
            try
            {
                int ID = 2;
                int occurrence1 = 1, occurrence2;
                DataSet ds = new DataSet();
                // Check amount of criminal records to {ID}
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter($"SELECT COUNT(RecordID) " +
                        $"FROM CriminalRecord " +
                        $"WHERE PersonID = {ID}", con);
                    dataAdapter.Fill(ds);
                    var table = ds.Tables[0];
                    occurrence1 = Int32.Parse(table.Rows[0][0].ToString());
                }

                Police police = Police.GetInstance();
                police.OpenCriminalRecord(ID);
                DataSet ds2 = new DataSet();
                // Check amount of criminal records to {ID} after OpenCriminalRecord func
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter($"SELECT COUNT(RecordID) " +
                        $"FROM CriminalRecord " +
                        $"WHERE PersonID = {ID}", con);
                    dataAdapter.Fill(ds2);
                    var table = ds2.Tables[0];
                    occurrence2 = Int32.Parse(table.Rows[0][0].ToString());
                }
                Assert.AreEqual(occurrence1, occurrence2 - 1);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
