using CoronaTrackerBL.Manager;
using CoronaTrackerBL.Others;
using CoronaTrackerDAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace UnitTestProjectCorona
{
    [TestClass]
    public class QuarantineMangerTests
    {
        readonly QuarantineManager quarantineManger = QuarantineManager.GetInstance();
        [TestMethod]
        public async Task CheckAddTQuarantine()
        {
            quarantineManger.AddToQuarantine(6, 1);
            quarantineManger.AddToQuarantine(7, 2);
            quarantineManger.AddToQuarantine(5, 3);
            await Task.Delay(3000);
            Assert.IsTrue(quarantineManger._personQuarantineByReleaseDate.ContainsKey(DateTime.UtcNow.Date.AddDays(14)));
            Assert.AreEqual(3, quarantineManger._personQuarantineByReleaseDate.GetOrAdd(DateTime.UtcNow.Date.AddDays(14), 0));
            quarantineManger.AddToQuarantine(3, DateTime.UtcNow.Date.AddDays(-3), 4);
            await Task.Delay(3000);
            Assert.IsTrue(quarantineManger._personQuarantineByReleaseDate.ContainsKey(DateTime.UtcNow.Date.AddDays(11)));
            Assert.AreEqual(1, quarantineManger._personQuarantineByReleaseDate.GetOrAdd(DateTime.UtcNow.Date.AddDays(11), 0));
            await Task.Delay(7000);
        }

        [TestMethod]
        public async Task CheckReleaseFromQuarantine()
        {
            var tempCount = quarantineManger._personQuarantineByReleaseDate.GetOrAdd(DateTime.UtcNow.Date.AddDays(14), 0);
            quarantineManger.AddToQuarantine(2, 5);
            await Task.Delay(3000);
            Assert.AreEqual(tempCount+1, quarantineManger._personQuarantineByReleaseDate.GetOrAdd(DateTime.UtcNow.Date.AddDays(14), 0));
            quarantineManger.ReleaseFromQuarantine(2);
            await Task.Delay(3000);
            Assert.AreEqual(tempCount, quarantineManger._personQuarantineByReleaseDate.GetOrAdd(DateTime.UtcNow.Date.AddDays(14), 0));
            await Task.Delay(7000);
        }

    }
}

