﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using CoronaTrackerBL.LaboratoriesManager;
using CoronaTrackerBL.Laboratory;
using CoronaTrackerBL.Other;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProjectCorona
{
    [TestClass]
    public class Laboratorie_ManagerTest
    {
        ILaboratoriesManager labManager = LaboratoriesManager.GetInstance();
        [TestMethod]
        public void EnoughCapacityAndKits()
        {
            ILaboratory lab1 = new Laboratory(1, new Location(1, 4), 7, labManager, 100, 0);
            ILaboratory lab2 = new Laboratory(2, new Location(2, 5), 7, labManager, 100, 0);
            labManager.Laboratories = new List<ILaboratory>();
            labManager.Laboratories.Add(lab1);
            labManager.Laboratories.Add(lab2);
            Person person = new Person(2, "check", 2, 5);
            Assert.AreEqual(lab2, labManager.GetPrioritizedLaboratory(person.Address));
        }
        [TestMethod]
        public void EnoughCapacityNoKits()
        {
            ILaboratory lab1 = new Laboratory(1, new Location(1, 4), 7, labManager, 100, 0);
            /*Type type = typeof(Laboratory);
            ConstructorInfo ctor1 = type.GetConstructor(new Type[]
            { typeof(int), typeof(Location), typeof(int), typeof(ILaboratoriesManager) });
            Object instance1 = ctor1.Invoke(new object[] { 2, new Location(2, 5), 7, labManager, 100, 0 });
            PropertyInfo prop = type.GetProperty("KitsInStock");
            prop.SetValue(instance1, 0, null);*/
            ILaboratory lab2 = new Laboratory(2, new Location(2, 5), 7, labManager, 100, 100);
            labManager.Laboratories = new List<ILaboratory>();
            labManager.Laboratories.Add(lab1);
            labManager.Laboratories.Add(lab2);
            Person person = new Person(2, "check", 2, 5);
            Assert.AreEqual(lab1, labManager.GetPrioritizedLaboratory(person.Address));
        }
        [TestMethod]
        public void EnoughKitsNoCapacity()
        {
            ILaboratory lab1 = new Laboratory(1, new Location(1, 4), 7, labManager, 100, 0);
            /*Type type = typeof(Laboratory);
            ConstructorInfo ctor1 = type.GetConstructor(new Type[]
            { typeof(int), typeof(Location), typeof(int), typeof(ILaboratoriesManager) });
            Object instance1 = ctor1.Invoke(new object[] { 2, new Location(2, 5), 7, labManager, 100, 0 });
            PropertyInfo prop = type.GetProperty("TestsCurrentInLab");
            prop.SetValue(instance1, lab1.MaxStocksPossible, null);*/
            ILaboratory lab2 = new Laboratory(2, new Location(2, 5), 7, labManager, 0, 0);
            labManager.Laboratories = new List<ILaboratory>();
            labManager.Laboratories.Add(lab1);
            labManager.Laboratories.Add(lab2);
            Person person = new Person(2, "check", 2, 5);
            Assert.AreEqual(lab1, labManager.GetPrioritizedLaboratory(person.Address));
        }
        [TestMethod]
        public void NoCapacityAndKits()
        {
            ILaboratory lab1 = new Laboratory(1, new Location(1, 4), 7, labManager, 100, 0);
            /*Type type = typeof(Laboratory);
            ConstructorInfo ctor1 = type.GetConstructor(new Type[]
            { typeof(int), typeof(Location), typeof(int), typeof(ILaboratoriesManager) });
            Object instance1 = ctor1.Invoke(new object[] { 2, new Location(2, 5), 7, labManager });
            PropertyInfo prop1 = type.GetProperty("TestsCurrentInLab");
            prop1.SetValue(instance1, lab1.MaxStocksPossible, null);
            PropertyInfo prop2 = type.GetProperty("KitsInStock");
            prop2.SetValue(instance1, 0, null);*/
            ILaboratory lab2 = new Laboratory(2, new Location(2, 5), 7, labManager, 0, 100);
            labManager.Laboratories = new List<ILaboratory>();
            labManager.Laboratories.Add(lab1);
            labManager.Laboratories.Add(lab2);
            Person person = new Person(2, "check", 2, 5);
            Assert.AreEqual(lab1, labManager.GetPrioritizedLaboratory(person.Address));
        }
        [TestMethod]
        public void AllLabsAreFull()
        {
            bool labsFull = false;
            try
            {
                /*Type type = typeof(Laboratory);
                ConstructorInfo ctor1 = type.GetConstructor(new Type[]
                { typeof(int), typeof(Location), typeof(int), typeof(ILaboratoriesManager) });
                Object instance1 = ctor1.Invoke(new object[] { 1, new Location(1, 4), 7, labManager });
                PropertyInfo prop = type.GetProperty("KitsInStock");
                prop.SetValue(instance1, 0, null);*/
                ILaboratory lab1 = new Laboratory(1, new Location(1, 4), 7, labManager, 0, 0);
                /*ConstructorInfo ctor2 = type.GetConstructor(new Type[]
                 { typeof(int), typeof(Location), typeof(int), typeof(ILaboratoriesManager) });
                Object instance2 = ctor2.Invoke(new object[] { 1, new Location(1, 4), 7, labManager });
                PropertyInfo prop2 = type.GetProperty("TestsCurrentInLab");
                prop2.SetValue(instance2, 100, null);*/
                ILaboratory lab2 = new Laboratory(2, new Location(2, 5), 7, labManager, 100, 100); ;
                labManager.Laboratories = new List<ILaboratory>();
                labManager.Laboratories.Add(lab1);
                labManager.Laboratories.Add(lab2);
                Person person = new Person(2, "check", 2, 5);
                if(labManager.GetPrioritizedLaboratory(person.Address)==null)
                    labsFull = true;
                Assert.IsTrue(labsFull);
            }
            catch
            {
                Console.WriteLine("problem with DB");
            }
            
        }

    }
}
