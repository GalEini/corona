﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using CoronaTrackerBL.LaboratoriesManager;
using CoronaTrackerBL.Laboratory;
using CoronaTrackerBL.Other;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProjectCorona
{
    [TestClass]
    public class LaboratoryTest
    {
        ILaboratoriesManager labManager = LaboratoriesManager.GetInstance();                
        [TestMethod]
        public void FreeForTestInfection()
        {
            ILaboratory lab = new Laboratory(1, new Location(1, 4), 7, labManager, 100, 0);
            Assert.IsTrue(lab.TestInfection(1, true));
        }
        [TestMethod]
        public void FullTestCapacityInTestInfection()
        {
            /*Type type = typeof(Laboratory);
            ConstructorInfo ctor1 = type.GetConstructor(new Type[]
            { typeof(int), typeof(Location), typeof(int), typeof(ILaboratoriesManager) });
            Object instance1 = ctor1.Invoke(new object[] { 1, new Location(1, 4), 7, labManager, 100, 0 });
            PropertyInfo prop = type.GetProperty("TestsCurrentInLab");
            prop.SetValue(instance1, 100, null);*/
            ILaboratory lab = new Laboratory(1, new Location(1, 4), 7, labManager, 100, 100); ;
            Assert.IsFalse(lab.TestInfection(1, true));
        }
        [TestMethod]
        public void EmptyKitStockInTestInfection()
        {
           /* Type type = typeof(Laboratory);
            ConstructorInfo ctor1 = type.GetConstructor( new Type[] 
            { typeof(int), typeof(Location), typeof(int), typeof(ILaboratoriesManager) });
            Object instance1 = ctor1.Invoke(new object[] { 1, new Location(1, 4), 7, labManager, 100, 0 });
            PropertyInfo prop = type.GetProperty("KitsInStock");
            prop.SetValue(instance1, 0, null);*/
            ILaboratory lab = new Laboratory(1, new Location(1, 4), 7, labManager, 0, 0);
            Assert.IsFalse(lab.TestInfection(1, true));
        }
    }
}
