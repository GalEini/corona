﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoronaTrackerBL.Manager
{
    public abstract class Manger
    {
        private CancellationTokenSource cts = null;
        private int _flagStart = 0;
        private Task _run = null;

 

        protected abstract Action OngoingOperation { get; }

        public void Start()
        {
            if (Interlocked.CompareExchange(ref _flagStart, 1, 0) == 0)
            {
                _run = Run(); ;
            }
        }
        public void Stop()
        {
            cts.Cancel();
            _flagStart = 0;
        }

        private Task Run()
        {
            var task = new Task(async () =>
            {
                cts = new CancellationTokenSource();
                try
                {
                    while (!cts.IsCancellationRequested)
                    {
                        var delayTask = Task.Delay(TimeSpan.FromDays(1), cts.Token);
                        OngoingOperation();
                        await delayTask;
                    }
                }
                catch (TaskCanceledException) { }
                catch (Exception ex)
                {

                }

            }, TaskCreationOptions.LongRunning);
            task.Start();
            return task;
        }

        

    }
}
