﻿using CoronaTrackerBL.LaboratoriesManager;
using CoronaTrackerBL.Laboratory;
using CoronaTrackerBL.Log;
using CoronaTrackerBL.Others;
using CoronaTrackerBL.Patient;
using CoronaTrackerDAL;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static CoronaTrackerBL.Log.Logger;

namespace CoronaTrackerBL.Manager
{
    public class InfectedManager : Manger
    {
        private static InfectedManager _instance = null;
        private static readonly object _padLock = new object();
        private Dictionary<long, int> _patients;
        //private CancellationTokenSource cts = null;
        //private int _flagStart = 0;
        private Task _checkPatientsTask;
        private ConcurrentQueue<long> _personToAddHospitalQueue;
        private int _flagQueue = 0;
        private ILaboratory _laboratory;
        public ILaboratoriesManager _laboratoriesManager;
        public ConcurrentDictionary<int, int> AllInfectedGroupByDays;
        private Task _runQueueActions = null;
        private ILogger _logger = null;

        protected override Action OngoingOperation => OngoingOperationCheckPatietns;

        private InfectedManager()
        {

        }
        public static InfectedManager GetInstance()
        {
            if (_instance == null)
            {
                lock (_padLock)
                {
                    if (_instance == null)
                    {
                        _instance = new InfectedManager();
                        _instance.Init();
                    }
                }
            }
            return _instance;
        }
        private void Init()
        {
            
            _logger = new Logger();
            _personToAddHospitalQueue = new ConcurrentQueue<long>();
            _patients = InfectedDAL.PopulateHospitalPatients();
            _laboratoriesManager = LaboratoriesManager.LaboratoriesManager.GetInstance();
            _laboratoriesManager.InfectionResult += HandleTestInfectionResult;
            _runQueueActions = HandleQueueThread();
            StartHospitalization();
            AllInfectedGroupByDays = new ConcurrentDictionary<int, int>(StatsDAL.GetAllInfecGroupByDays());
            InitAfterCrash();
        }
        public void InitAfterCrash()
        {
            Task.Factory.StartNew(() =>
            {
                var infoAfterCrash = InfectedDAL.InitInfectedManger();
                if (infoAfterCrash != null)
                {
                    foreach (var item in infoAfterCrash)
                    {
                        HandleTestInfectionResult(new TestEventArgs (item.personID, item.result), item.eventID);
                    }
                }
                
            });
        }
        public void HospitalizePatient(long personID)
        {
            _patients.Add(personID, 1);
            if (AllInfectedGroupByDays.ContainsKey(1))
            {
                AllInfectedGroupByDays[1]++;
            }
            else
            {
                AllInfectedGroupByDays.TryAdd(1, 1);
            }
            _personToAddHospitalQueue.Enqueue(personID);
        }

        public Task HandleQueueThread()
        {
            var iterateQueuetask = new Task(async () =>
            {
                while (true)
                {
                    if (_personToAddHospitalQueue.TryDequeue(out long result))
                    {
                        InfectedDAL.AddPersonToHospital(result);
                    }
                    else
                    {
                        await Task.Delay(100);
                    }
                }
            }, TaskCreationOptions.LongRunning);
            iterateQueuetask.Start();
            return iterateQueuetask;
        }
        public void DischargePatient(long personID)
        {
            AllInfectedGroupByDays[_patients[personID]]--;
            _patients.Remove(personID);
            InfectedDAL.ReleasePersonFromHospDB(personID);
        }
        public void StartHospitalization()
        {
            Start();
        }
        public void StopHospitalization()
        {
            Stop();
        }
        public void OngoingOperationCheckPatietns()
        {                       
            foreach (long personId in _patients.Keys)
            {
                Random rnd = new Random();
                int daysSick = _patients[personId];
                bool result;
                if (daysSick >= 20)
                {
                    result = true;
                }
                else
                {
                    result = rnd.Next(0, 100) < daysSick * 0.05 ? true : false;
                }
                _laboratoriesManager.TestInfection(personId, result);
            }
            InfectedDAL.IncrementSickDays();
            IncreaseDaysInmemorey();
        }

        public void IncreaseDaysInmemorey()
        {
            foreach (int day in AllInfectedGroupByDays.Keys)
            {
                var count = AllInfectedGroupByDays[day];
                AllInfectedGroupByDays.TryRemove(day, out int removed);
                if (removed != 0)
                {
                    AllInfectedGroupByDays.TryAdd(day + 1, count);
                }
            }

        }
        public void HandleTestInfectionResult(TestEventArgs args,int eventID)
        {
            if (args.Result)
            {
                if (!_patients.ContainsKey(args.PersonID))
                {
                    HospitalizePatient(args.PersonID);
                }
            }
            else
            {
                if (_patients.ContainsKey(args.PersonID))
                {
                    DischargePatient(args.PersonID);
                }
            }
            _logger.LogReaction(eventID, EReaction.HospitalOrReleasePatient);

        }

    }
}
