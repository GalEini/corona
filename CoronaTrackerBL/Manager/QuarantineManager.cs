﻿using CoronaTrackerBL.Other;
using CoronaTrackerBL.Patient;
using CoronaTrackerBL.LaboratoriesManager;
using CoronaTrackerBL.Police;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using CoronaTrackerDAL;
using CoronaTrackerBL.Others;
using System.Linq;
using static CoronaTrackerBL.Manager.QueueActionItem;
using CoronaTrackerBL.Log;

namespace CoronaTrackerBL.Manager
{
    public class QuarantineManager : Manger
    {
        private static QuarantineManager _instance = null;
        private static readonly object _padLock = new object();
        private static readonly object _padLockSoonToRelease = new object();
        private ConcurrentDictionary<long, DateTime> _soonToRelease { get; set; }
        public event Action<long, int> quarantineEnded;
        public ConcurrentDictionary<DateTime, int> _personQuarantineByReleaseDate;
        private ConcurrentQueue<QueueActionItem> _queueQuaratineManger;
        private Task _runQueueActions = null;
        private ILogger _logger;

        protected override Action OngoingOperation => OngoingOperationMethod;


        private QuarantineManager()
        {

        }
        public static QuarantineManager GetInstance()
        {
            if (_instance == null)
            {
                lock (_padLock)
                {
                    if (_instance == null)
                    {
                        _instance = new QuarantineManager();
                        _instance.Init();
                    }
                }
            }
            return _instance;
        }
        private void Init()
        {
            _logger = new Logger();
            
            var laboratoriesManager = LaboratoriesManager.LaboratoriesManager.GetInstance();
            laboratoriesManager.InfectionResult += HandleTestInfectionResult;
            IPolice police = Police.Police.GetInstance();
            police.UnauthorizedWalker += AddToQuarantine;
            police.FoundCompromised += AddToQuarantine;
            _queueQuaratineManger = new ConcurrentQueue<QueueActionItem>();
            PopulateSoonToRelease();
            _personQuarantineByReleaseDate = new ConcurrentDictionary<DateTime, int>(StatsDAL.GetAllQuarGroupByDate());
            _runQueueActions = HandleQueueThread();
            StartQuarantine();
            InitAfterCrash();
        }

        public void InitAfterCrash()
        {
            Task.Factory.StartNew(() =>
            {
                var infoAfterCrash = QuarantineDAL.InitQuarantinManger();
                if (infoAfterCrash.AddtoQuarantineList != null)
                {
                    foreach (var item in infoAfterCrash.AddtoQuarantineList)
                    {
                        AddToQuarantine(item.personID, item.reportDate, item.eventID);
                    }
                }
                if (infoAfterCrash.ReleaseFromQuarnList != null)
                {
                    foreach (var item2 in infoAfterCrash.ReleaseFromQuarnList)
                    {
                        HandleTestInfectionResult(new TestEventArgs(item2.personID, item2.result), item2.eventID);
                    }
                }
            });
        }
        private void PopulateSoonToRelease()
        {
            if (_soonToRelease == null)
            {
                _soonToRelease = new ConcurrentDictionary<long, DateTime>(QuarantineDAL.PopulateSoonToRelease());
            }
            else
            {
                var tempQuarantine = QuarantineDAL.PopulateSoonToRelease();
                lock (_padLockSoonToRelease)
                {
                    foreach (var person in tempQuarantine)
                    {
                        if (!_soonToRelease.ContainsKey(person.Key))
                            _soonToRelease.TryAdd(person.Key, person.Value);
                    }
                }
            }
        }
        public void AddToQuarantine(long personID, int eventID) => AddToQuarantine(personID, DateTime.UtcNow.Date, eventID);

        public void AddToQuarantine(long personID, DateTime effectiveDate, int eventID)
        {
            var releaseDate = effectiveDate.AddDays(14);
            QueueActionItem person = new QueueActionItem(releaseDate, personID, ActionQueue.Add);
            if (_personQuarantineByReleaseDate.ContainsKey(releaseDate))
            {
                _personQuarantineByReleaseDate[releaseDate]++;
            }
            else
            {
                _personQuarantineByReleaseDate.TryAdd(releaseDate, 1);
            }
            _queueQuaratineManger.Enqueue(person);
            _logger.LogReaction(eventID, EReaction.AddToQuarantine);
        }

        public void ReleaseFromQuarantine(long personID)
        {
            QueueActionItem person = new QueueActionItem(DateTime.Today, personID, ActionQueue.Remove);

            if (_soonToRelease?.ContainsKey(personID) == true)
            {
                lock (_padLockSoonToRelease)
                {
                    _personQuarantineByReleaseDate[_soonToRelease[personID]]--;
                    _soonToRelease.TryRemove(personID, out DateTime date);
                }
            }
            else
            {
                var date = QuarantineDAL.GetDaysOfPerson(personID);
                if (date != null)
                {
                    DateTime tempDate = new DateTime(date.Value.Year, date.Value.Month, date.Value.Day);
                    _personQuarantineByReleaseDate[tempDate]--;
                }
            }
            _queueQuaratineManger.Enqueue(person);
        }
        private Task HandleQueueThread()
        {
            var iterateActionQueuetask = new Task(async () =>
            {

                while (true)
                {
                    if (_queueQuaratineManger.TryDequeue(out QueueActionItem result))
                    {
                        if (result.Action == ActionQueue.Remove)
                        {
                            QuarantineDAL.ReleasePersonFromQuaDB(result.PersonId);
                        }
                        else
                        {
                            QuarantineDAL.AddPersonToDB(result.PersonId, result.RealeaseDate);
                        }
                    }
                    else
                    {
                        await Task.Delay(1000);
                    }

                }
            }, TaskCreationOptions.LongRunning);
            iterateActionQueuetask.Start();
            return iterateActionQueuetask;
        }

        public void StartQuarantine()
        {
            Start();
        }
        public void StopQuarantine()
        {
            Stop();
        }

        public void OngoingOperationMethod()
        {
            if (_soonToRelease != null)
            {
                foreach (var person in _soonToRelease)
                {
                    if (person.Value.Date <= DateTime.UtcNow.Date)
                    {
                        var eventID = _logger.LogQuarantineEndedEvent(person.Key);
                        quarantineEnded?.Invoke(person.Key, eventID);
                    }
                }
            }
            PopulateSoonToRelease();
        }

        public void HandleTestInfectionResult(TestEventArgs args, int eventID)
        {
            ReleaseFromQuarantine(args.PersonID);
            _logger.LogReaction(eventID, EReaction.ReleaseFromQuarantin);
        }
    }
    public class QueueActionItem
    {
        public QueueActionItem(DateTime untilDate, long personId, ActionQueue action)
        {
            RealeaseDate = untilDate;
            PersonId = personId;
            Action = action;
        }

        public DateTime RealeaseDate { get; set; }
        public long PersonId { get; set; }
        public ActionQueue Action { get; set; }

        public enum ActionQueue
        {
            Remove = 0,
            Add = 1
        }
    }
}

