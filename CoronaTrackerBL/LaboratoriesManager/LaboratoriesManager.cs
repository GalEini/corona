﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoronaTrackerBL.Laboratory;
using CoronaTrackerBL.Log;
using CoronaTrackerBL.Manager;
using CoronaTrackerBL.Other;
using CoronaTrackerBL.Others;
using CoronaTrackerBL.Police;
using CoronaTrackerDAL;
using static CoronaTrackerBL.Log.Logger;

namespace CoronaTrackerBL.LaboratoriesManager
{
    public class LaboratoriesManager : ILaboratoriesManager
    {
        private static LaboratoriesManager _instance = null;
        private static readonly object _padLock = new object();
        private LaboratoriesManager()
        {

        }
        public static LaboratoriesManager GetInstance()
        {
            if (_instance == null)
            {
                lock (_padLock)
                {
                    if (_instance == null)
                    {
                        _instance = new LaboratoriesManager();
                        _instance.Init();
                    }
                }
            }
            return _instance;
        }

        private void Init()
        {
            _logger = new Logger();
            QuarantineManager quarantineManager = QuarantineManager.GetInstance();
            quarantineManager.quarantineEnded += _instance.HandleTestInfection;
            IPolice police = Police.Police.GetInstance();
            police.FoundCompromised += _instance.HandlePoliceEvents;
            police.UnauthorizedWalker += _instance.HandlePoliceEvents;
            var initLabs = LaboratoriesManagerDAL.GetLaboratories();
            if (initLabs != null)
            {
                foreach (var (LaboratoryID, Laboratorians, addressLat, addressLongt, kits, test) in initLabs)
                {
                    Laboratories.Add(new Laboratory.Laboratory(LaboratoryID, new Location(addressLat, addressLongt),
                        Laboratorians, this, kits, test));
                }
            }
            InitAfterCrash();
            
        }
        public event TestResultEvent InfectionResult;
        public List<ILaboratory> Laboratories { get ; set ; }
        
        private ILogger _logger;
        private IPeopleManager _peopleManeger = UnlimitedCachePeopleManager.GetInstance();
        private Random _rand = new Random();
        public ILaboratory GetPrioritizedLaboratory(Location location)
        {
            if (Laboratories.Count != 0)
            {
                ILaboratory closestLab = Laboratories[0];
                foreach (var lab in Laboratories)
                {
                    if (lab.KitsInStock > 0 && lab.TestsCurrentInLab < lab.MaxStocksPossible)
                        if (lab.LabLocation.Distance(location) < closestLab.LabLocation.Distance(location))
                            closestLab = lab;
                }
                return closestLab;
            }
            return null;
        }

        public void InitAfterCrash()
        {
            Task.Factory.StartNew(() =>
            {
                var infoAfterCrash = LaboratoriesManagerDAL.InitlabsManager();
                if (infoAfterCrash.foundCompromisedList != null)
                {
                    foreach (var compromised in infoAfterCrash.foundCompromisedList)
                    {
                        HandlePoliceEvents(compromised.personID, compromised.reportDate, compromised.EventID);
                    }
                    foreach (var unauthorized in infoAfterCrash.unauthorizedList)
                    {
                        HandlePoliceEvents(unauthorized.personID, unauthorized.reportDate, unauthorized.EventID);
                    }
                    foreach (var quarantinedID in infoAfterCrash.quarantineEndedList)
                    {
                        HandleTestInfection(quarantinedID.personID, quarantinedID.EventID);
                    }
                }
            });
        }
        public void HandleTestInfection(long personID,int eventID)
        {
            TestInfection(personID);
            _logger.LogReaction(eventID, EReaction.TestInfection);
        }
        public void TestInfection(long personID) 
        {
            TestInfection(personID, Convert.ToBoolean(_rand.Next(0,2)));
        }
        public void TestInfection(long personID, bool evetualOutcome)
        {
            var personAdress = _peopleManeger.GetPerson(personID).Address;
            if (personAdress == null)
            {
                Console.WriteLine($"Couldn't find  {personID} in the DB"); 
                return;
            }
            ILaboratory lab = GetPrioritizedLaboratory(personAdress);
            if (lab == null)
            {
                Console.WriteLine($"Couldn't find availible labs for {personID}");
                return;
            }
            if (!lab.TestInfection(personID, evetualOutcome))
            {
                Console.WriteLine($"Test infection for {personID} has failed");
            }
        }
        public void HandlePoliceEvents(long personID, DateTime encounterDate,int eventID)
        {
            TestInfection(personID);
            _logger.LogReaction(eventID, EReaction.TestInfection);

        }
        public void InvokeInfectionResult(TestEventArgs args)
        {
            var eventID = _logger.LogInfectionResultEvent(args);
            InfectionResult.Invoke(args,eventID);
        }
        
    }

    

}
