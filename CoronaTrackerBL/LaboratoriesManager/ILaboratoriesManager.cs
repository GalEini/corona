﻿using System;
using System.Collections.Generic;
using System.Text;
using CoronaTrackerBL.Laboratory;
using CoronaTrackerBL.Other;
using CoronaTrackerBL.Others;

namespace CoronaTrackerBL.LaboratoriesManager
{
    public delegate void TestResultEvent(TestEventArgs args,int eventID);
    public interface ILaboratoriesManager
    {
        List<ILaboratory> Laboratories { get; set; }
        ILaboratory GetPrioritizedLaboratory(Location location);
        void TestInfection(long personID);
        void TestInfection(long personID, bool evetualOutcome);
        void InvokeInfectionResult(TestEventArgs args);
        event TestResultEvent InfectionResult;
    }
}
