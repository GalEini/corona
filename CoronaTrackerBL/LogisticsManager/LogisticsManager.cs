﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoronaTrackerBL.Log;

namespace CoronaTrackerBL.LogisticsManager
{
    public class LogisticsManager : ILogisticsManager
    {
        private static LogisticsManager _instance = null;
        private int _kitsAmount;
        private int _kitsDistrbuted;
        private Tuple<DateTime, int> _currentOrder = null;
        
        public int KitsAmount
        { 
            get 
            {
                return _kitsAmount;
            }
             private set
            {
                _kitsAmount = value;
            }
        }
        public Tuple<DateTime, int> CurrentOrder
        {
            get
            {
                return _currentOrder;
            }
            private set
            {
                _currentOrder = value;
            }
        }
        public int KitsDistrbuted
        {
            get
            {
                return _kitsDistrbuted;
            }
            private set
            {
                _kitsDistrbuted = value;
            }
        }
        private LogisticsManager()
        {
            _kitsAmount = STOCK_ORDER;
            KitsDistrbuted = 0;
        }
        public bool HasOrdered()
        {
            return _isOrderOngoing == 1;            
        }
        

        private const int STOCK_ORDER = 1500;
        private const int MIN_TO_ORDER = 1000;
        private static readonly object _padLock = new object();
        private int _isOrderOngoing = 0;
        private Task _orderTask;
        public static LogisticsManager GetInstance()
        {            
            if (_instance == null)
            {
                lock (_padLock)
                {
                    if (_instance == null)
                    {
                        _instance = new LogisticsManager();
                    }
                }
                
            }
            return _instance;
        }

        public void OrderTestKitsToStock(int amount)
        {
            //interlocked
            if (Interlocked.CompareExchange(ref _isOrderOngoing, 1, 0) == 0)
            {
                _orderTask = OrderTestKitsToStock_Inner(amount);
            }
        }
        
        private  Task OrderTestKitsToStock_Inner(int amount)
        {
            return Task.Factory.StartNew(async () =>
            {
                try
                {
                    Random rand = new Random();
                    int days = rand.Next(1, 4);
                    await Task.Delay(TimeSpan.FromDays(days));
                    this._kitsAmount += amount;
                    CurrentOrder = null;
                    _orderTask = null;
                }
                finally
                {
                    _isOrderOngoing = 0;
                }
            }, TaskCreationOptions.LongRunning);
        }      

        public int WithdrawTestKits(int amount)
        {
            int giveKits;
            bool exchanged;
            // Withdraw the possible kits
            do
            {
                int stock = this.KitsAmount;
                giveKits = Math.Min(stock, amount);
                int newStock = stock - giveKits;
                int originalValue = Interlocked.CompareExchange(ref _kitsAmount, newStock, stock);
                exchanged = (originalValue == stock);
            } while (!exchanged);
            if(this.KitsAmount < MIN_TO_ORDER)
                OrderTestKitsToStock(STOCK_ORDER - this._kitsAmount);
            Interlocked.Add(ref _kitsDistrbuted, giveKits);
            return giveKits;

        }
        public void RemoveTestKits(int amount)
        {
            WithdrawTestKits(amount);
        }
    }
}
