﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoronaTrackerBL.LogisticsManager
{
    public interface ILogisticsManager
    {
        int KitsAmount { get; }
        int WithdrawTestKits(int amount);
        void OrderTestKitsToStock(int amount);
        bool HasOrdered();
        void RemoveTestKits(int amount);
    }
}
