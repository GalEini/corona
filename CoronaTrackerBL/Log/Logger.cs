﻿using System;
using System.Collections.Generic;
using System.Text;
using CoronaTrackerBL.Others;
using CoronaTrackerDAL;

namespace CoronaTrackerBL.Log
{
    
    class Logger : ILogger
    {
        public int LogFoundCompromisedEvent(long personID, DateTime dateComp)
        {
            return LoggerDAL.LogFoundCompromisedEvent(personID, dateComp);
        }

        public int LogInfectionResultEvent(TestEventArgs args)
        {
            return LoggerDAL.LogInfectionResultEvent(args.PersonID, args.Result);
        }

        public int LogQuarantineEndedEvent(long personID)
        {
            return LoggerDAL.LogQuarantineEndedEvent(personID);
        }

        public void LogReaction(int eventId, EReaction enumFuture)
        {
            LoggerDAL.LogReaction(eventId, (int)enumFuture);
        }

        public int LogUnauthorizedWalkerEvent(long personID, DateTime reportDate)
        {
            return LoggerDAL.LogUnauthorizedWalkerEvent(personID, reportDate);
        }
    }
}
