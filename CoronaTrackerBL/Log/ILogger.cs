﻿using System;
using System.Collections.Generic;
using System.Text;
using CoronaTrackerBL.Others;

namespace CoronaTrackerBL.Log
{
    public enum EReaction
    {
        AddToQuarantine = 1,
        ReleaseFromQuarantin = 2,
        HospitalOrReleasePatient = 3,
        OpenCriminalRecord = 4,
        SearchForCompromized = 5,
        TestInfection = 6
    }
    interface ILogger
    {
        //void Log(string text);
        void LogReaction(int eventId, EReaction enumFuture);
        int LogInfectionResultEvent(TestEventArgs args);
        int LogFoundCompromisedEvent(long personID, DateTime dateComp);
        int LogUnauthorizedWalkerEvent(long personID, DateTime reportDate);
        int LogQuarantineEndedEvent(long personID);
    }
    
}
