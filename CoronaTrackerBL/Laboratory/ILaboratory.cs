﻿using System;
using System.Collections.Generic;
using System.Text;
using CoronaTrackerBL.LogisticsManager;
using CoronaTrackerBL.Other;

namespace CoronaTrackerBL.Laboratory
{
    public interface ILaboratory
    {
        int ID { get; set; }
        Location LabLocation{ get; set; }
        int KitsInStock { get; }
        int Laboratorians { get; set; }
        int TestsCurrentInLab { get;  }
        ILogisticsManager LogisticsManager { get; set; }
        bool TestInfection(long personID, bool evetualOutcome);
        int MaxStocksPossible { get; set; }
    }
}
