﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoronaTrackerBL.LaboratoriesManager;
using CoronaTrackerBL.Log;
using CoronaTrackerBL.LogisticsManager;
using CoronaTrackerBL.Other;
using CoronaTrackerBL.Others;

namespace CoronaTrackerBL.Laboratory
{
    public class Laboratory : ILaboratory
    {       
        public int ID { get ; set ; }
        public Location LabLocation { get; set; }
        public int Laboratorians { get; set; }
        public int KitsInStock { 
            get
            {
                return _kitsInStock;
            }
            private set
            {
                _kitsInStock = value;
            }
        }        
        public int TestsCurrentInLab
        {
            get
            {
                return _testsCurrentInLab;
            }
            private set
            {
                _testsCurrentInLab = value;

            }
        }
        public int TestsFinishedInLab
        {
            get
            {
                return _testsFinishedInLab;
            }
            set
            {
                _testsFinishedInLab = value;

            }
        }
        public int TestsRejectedInLab
        {
            get
            {
                return _testsRejectedTInLab;
            }
            set
            {
                _testsRejectedTInLab = value;

            }
        }
        public ILogisticsManager LogisticsManager { get; set; }
        public int MaxStocksPossible { get; set; }
        private static readonly object _padLock = new object();
        private int _kitsInStock;
        private int _testsCurrentInLab;
        private int _testsFinishedInLab;
        private int _testsRejectedTInLab;
        private const int MAX_CAPACITY = 100;
        private const int WITHRAWAL_NUMBER = 100;
        private readonly SemaphoreSlim  _laboratoriansLock;
        private readonly ILaboratoriesManager _labManeger;
        private Task _convetialVar;
        public Laboratory(int Id, Location location, int laboratorians, ILaboratoriesManager labManeger,int kits, int test)
        {
            ID = Id;
            LabLocation = location;
            Laboratorians = laboratorians;
            TestsCurrentInLab = test;
            LogisticsManager = CoronaTrackerBL.LogisticsManager.LogisticsManager.GetInstance();
            KitsInStock = kits;
            _laboratoriansLock = new SemaphoreSlim(laboratorians);
            MaxStocksPossible = MAX_CAPACITY;
            _labManeger = labManeger;
        }
        public bool TestInfection(long personID, bool evetualOutcome)
        {
            // Check if there're too many tests in the lab and take the resource if there aren't.
            if (!ThreadSafe.ThreadSafeLimit(ref this._testsCurrentInLab, +1, MAX_CAPACITY))
                return false;
            // Check if there're enough tests in the lab and take the resource if there are.
            if (!ThreadSafe.ThreadSafeLimit(ref this._kitsInStock, -1, 0))
            {
                Interlocked.Decrement(ref _testsCurrentInLab);
                return false;
            }
            _convetialVar = WorkOnTest(personID, evetualOutcome);
            if (KitsInStock == 0)
                lock (_padLock) {
                    if (KitsInStock == 0)
                        Interlocked.Add(ref _kitsInStock, LogisticsManager.WithdrawTestKits(WITHRAWAL_NUMBER));
                }
            return true;
        }

        private Task WorkOnTest(long personID, bool evetualOutcome)
        {
            return Task.Factory.StartNew(() =>
            {
                //_logger.Log($"testInfaction, start, {personID}");
                // Enter if there're free laboratorians
                _laboratoriansLock.Wait();
                Task.Delay(TimeSpan.FromMinutes(30)); //30 minutes
                _laboratoriansLock.Release();
                Task.Delay(TimeSpan.FromHours(3)); // 3 hours
                Interlocked.Decrement(ref _testsCurrentInLab);
               // _logger.Log($"testInfaction, stop, {personID}");
                _labManeger.InvokeInfectionResult(new TestEventArgs(personID, evetualOutcome));
            }, TaskCreationOptions.LongRunning);
        }
    }
}