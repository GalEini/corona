﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoronaTrackerBL.Patient
{
    public class PersonInfected
    {

        public PersonInfected(long personID, int daysSinceDiagnosis)
        {
            PersonID = personID;
            DaysSinceDiagnosis = daysSinceDiagnosis;
            Status = true;
        }

        public long PersonID { get; }
        public int DaysSinceDiagnosis { get ; set ; }
        public bool Status { get; set ; }
    }
}
