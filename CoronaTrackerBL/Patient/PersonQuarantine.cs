﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoronaTrackerBL.Patient
{
    public class PersonQuarantine
    {
        public PersonQuarantine(long personID, DateTime realeaseDate)
        {
            PersonID = personID;
            RealeaseDate = realeaseDate;
            Status = true;
        }

        public long PersonID { get; }
        public DateTime RealeaseDate { get; set; }
        public bool Status { get; set; }
    }
}
