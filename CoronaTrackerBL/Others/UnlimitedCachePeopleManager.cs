﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using CoronaTrackerBL.Log;
using CoronaTrackerBL.Other;

namespace CoronaTrackerBL.Others
{
    public class UnlimitedCachePeopleManager : IPeopleManager
    {
        private Dictionary<long, Person> IDToPersonDictionary;
        private static UnlimitedCachePeopleManager _instance = null;
        private UnlimitedCachePeopleManager()
        {
            IDToPersonDictionary = new Dictionary<long, Person>();
        }
        private static readonly object padLock = new object();
        public static UnlimitedCachePeopleManager GetInstance()
        {
            if (_instance == null)
            {
                lock (padLock)
                {
                    if (_instance == null)
                    {
                        _instance = new UnlimitedCachePeopleManager();
                    }
                }

            }
            return _instance;
        }
        public Person GetPerson(long ID)
        {
            if (IDToPersonDictionary.ContainsKey(ID))
                return IDToPersonDictionary[ID];
            try
            {
                var info = CoronaTrackerDAL.IDToPersonDAL.GetPerson(ID);
                Person person = new Person(info.personID, info.name, info.addressLat, info.addressLongt);
                IDToPersonDictionary.Add(ID, person);
                return person;
            }
            catch (Exception)
            {
                return null;
            }

        }

}
    
}
