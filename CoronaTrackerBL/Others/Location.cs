﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoronaTrackerBL.Other
{
    public class Location
    {
        public int Longitude { get; private set; }
        public int Latitude { get; private set; }

        public Location(int latitude, int longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public bool Equals(Location loc)
        {
            return Latitude == loc.Latitude &&
                   Longitude == loc.Longitude;
        }

        public int Distance(Location loc)
        {
            return (int) Math.Sqrt(Math.Pow(Latitude - loc.Latitude, 2) + Math.Pow(Longitude - loc.Longitude, 2));
        }
    }
}
