﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace CoronaTrackerBL.Others
{
    public static class ThreadSafe
    {
        private const int MAX_ATTEMPTS = 1000;
        public static bool ThreadSafeLimit(ref int changeble, int change, int limit)
        {
            int iteration = 0;
            bool exchanged;
            // Check if there're too many tests in the lab and take the resource if there aren't.
            do
            {
                int tests = changeble;
                if (tests == limit || iteration == MAX_ATTEMPTS)
                    return false;
                int newTests = tests + change;
                int originalValue = Interlocked.CompareExchange(ref changeble, newTests, tests);
                exchanged = (originalValue == tests);
                iteration++;
            } while (!exchanged);
            return true;
        }
    }
}
