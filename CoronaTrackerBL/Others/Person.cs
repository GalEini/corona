﻿using System;
using System.Drawing;

namespace CoronaTrackerBL.Other
{
    public class Person
    {
        public string Name { get ; set ; }
        public long Id { get ; set ; }
        public Location Address { get ; set ; }

        public Person(long id, string name, int addressX, int addressY)
        {
            Id = id;
            Name = name;
            Address =  new Location (addressX, addressY);
        }

        
    }
}
