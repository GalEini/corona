﻿using System;
using System.Collections.Generic;
using System.Text;
using CoronaTrackerBL.Other;

namespace CoronaTrackerBL.Others
{
    interface IPeopleManager
    {
        Person GetPerson(long ID);
    }
}
