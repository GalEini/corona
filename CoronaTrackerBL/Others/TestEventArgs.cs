﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoronaTrackerBL.Others
{
    public class TestEventArgs : EventArgs
    {
        public long PersonID { get; set; }
        public bool Result { get; set; }
        public TestEventArgs(long personID, bool result)
        {
            PersonID = personID;
            Result = result;
        }
    }
}
