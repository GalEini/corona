﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoronaTrackerBL.Others;

namespace CoronaTrackerBL.Police
{
    //public delegate void TestResultEvent(TestEventArgs args);
    public delegate void PoliceActionEvent(long personID, DateTime encounterDate,int eventID);
    //public delegate void PoliceUnAuthorizedEventEvent(long personID);
    public interface IPolice
    {
        Dictionary<long,(Task, CancellationTokenSource)> CompromisedDictionary { get; set; }
        void InvokeCompromised(List<long> peopleIDs);
        Dictionary<long, List<long>> IDToCompromised { get; set; }
        bool ForceQuarantine(long personID);
        void SearchForCompromised(long personID);
        void OpenCriminalRecord(long personID);
        void ReportCompromize(long personID,DateTime dateComp);
        event PoliceActionEvent FoundCompromised;
        event PoliceActionEvent UnauthorizedWalker;
    }
}
