﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoronaTrackerBL.LaboratoriesManager;
using CoronaTrackerBL.Log;
using CoronaTrackerBL.Others;
using CoronaTrackerDAL;

namespace CoronaTrackerBL.Police
{
    public class Police : IPolice
    {
        public event PoliceActionEvent FoundCompromised;
        public event PoliceActionEvent UnauthorizedWalker;
        private Random _rand;       
        private static Police _instance = null;
        private static readonly object _padLock = new object();
        private ILogger _logger;
        private BlockingCollection<(long,int)> _criminalRecordQueue;
        public Dictionary<long, List<long>> IDToCompromised { get; set; }
        public Dictionary<long, (Task, CancellationTokenSource)> CompromisedDictionary { get; set; }
        private Task _convetialVarSearchCompromised;
        private Task _convetialVarCriminalRecord;
        private Police()
        {
            
        }
        private void Init()
        {
            _logger = new Logger();
            ILaboratoriesManager labManager = LaboratoriesManager.LaboratoriesManager.GetInstance();
            UnauthorizedWalker += HandleUnAuthorizedEvent;
            labManager.InfectionResult += HandleTestEvent;
            IDToCompromised = new Dictionary<long, List<long>>();
            CompromisedDictionary = new Dictionary<long, (Task, CancellationTokenSource)>();
            _criminalRecordQueue = new BlockingCollection<(long,int)>();
            InitCriminalRecordTask();
            InitAfterCrash();
        }
        public static Police GetInstance()
        {
            if (_instance == null)
            {
                lock (_padLock)
                {
                    if (_instance == null)
                    {
                        _instance = new Police();
                        _instance.Init();
                    }
                }
            }
            return _instance;
        }
        public void InitAfterCrash()
        {
            Task.Factory.StartNew(() =>
            {
                var infoAfterCrash = PoliceDAL.InitPolice();
                if (infoAfterCrash.unauthorizedList != null)
                {
                    foreach (var unauthorized in infoAfterCrash.unauthorizedList)
                    {
                        HandleUnAuthorizedEvent(unauthorized.personID, unauthorized.reportDate, unauthorized.EventID);
                    }
                    foreach (var infection in infoAfterCrash.infectionList)
                    {
                        HandleTestEvent(new TestEventArgs(infection.personID, infection.result), infection.EventID);
                    }
                }
            });
        }
        public bool ForceQuarantine(long personID)
        {
           // _logger.Log($"start force quarentine for person {personID}");
            var rndDate = RandomDate();
            var eventID = _logger.LogUnauthorizedWalkerEvent(personID, rndDate);
            UnauthorizedWalker.Invoke(personID, rndDate, eventID);
            
            return true;
        }
        public void InitCriminalRecordTask()
        {
            _convetialVarCriminalRecord = Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    var personEvent = _criminalRecordQueue.Take();
                    OpenCriminalRecord(personEvent.Item1);
                    _logger.LogReaction(personEvent.Item2, EReaction.OpenCriminalRecord);
                }
            }, TaskCreationOptions.LongRunning);
        }
        public void OpenCriminalRecord(long personID)
        {
            PoliceDAL.OpenCriminalRecord(personID);
        }

        public void SearchForCompromised(long personID)
        {
            var cts = new CancellationTokenSource();
            Task task = new Task(async () =>
            {
                _rand = new Random();
                int amountCompromized = _rand.Next(0, 7);
                List<long> peopleIDs = null;
                _convetialVarSearchCompromised = Task.Factory.StartNew(() =>
                {
                    peopleIDs = PoliceDAL.ListOfIDs(amountCompromized);                    
                });
                await Task.Delay(TimeSpan.FromDays(2), cts.Token);
                /***           
                 * return random 'amountCompromized' id's from DB
                 ***/
                if (cts.IsCancellationRequested)
                {
                    peopleIDs = IDToCompromised[personID];
                    IDToCompromised.Remove(personID);
                }
                    
                InvokeCompromised(peopleIDs);
                CompromisedDictionary.Remove(personID);
            }, TaskCreationOptions.LongRunning);
            CompromisedDictionary.Add(personID,(task, cts));
            CompromisedDictionary[personID].Item1.Start();
        }
        public void InvokeCompromised(List<long> peopleIDs)
        {
            foreach (var person in peopleIDs)
            {
                DateTime encounterDate = RandomDate();
                var eventID = _logger.LogFoundCompromisedEvent(person, encounterDate);
                FoundCompromised.Invoke(person, encounterDate, eventID);
            }
        }
        public void HandleTestEvent(TestEventArgs args,int eventID)
        {
            if (args.Result)
            {
                SearchForCompromised(args.PersonID);
            }
            _logger.LogReaction(eventID,EReaction.SearchForCompromized);
        }
        public void HandleUnAuthorizedEvent(long personID, DateTime encounterDate,int eventID)
        {
            _criminalRecordQueue.Add((personID,eventID));
        }
        public DateTime RandomDate()
        {
            DateTime start = new DateTime(2020, 2, 14);
            int range = ((TimeSpan)(DateTime.Today - start)).Days;
            return start.AddDays(_rand.Next(range));
        }

       /* public void ReportUnauthorizedWalker(long personID, DateTime reportDate)
        {
            var eventID = _logger.LogUnauthorizedWalkerEvent(personID, reportDate);
            UnauthorizedWalker.Invoke(personID, reportDate, eventID);
        }*/

        public void ReportCompromize(long personID,DateTime dateComp)
        {
            var eventID = _logger.LogFoundCompromisedEvent(personID,dateComp);
            FoundCompromised.Invoke(personID, dateComp, eventID);
        }
        
    }
}
