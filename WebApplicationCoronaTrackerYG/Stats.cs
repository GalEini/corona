﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationCoronaTrackerYG
{
    [Serializable]
    public class Stats
    {
        [JsonProperty("totalKits")]
        public int TotalKits { get; set; }
        public int TotalTestFinished { get; set; }
        public int TotalTestRejected { get; set; }
        public int TotalTestInProcess { get; set; }
        public List<LabStats> Laboratories { get; set; }
        public int WithdrawKits { get; set; }
        public int InventoryTotalKits { get; set; }
        public Tuple<DateTime, int> CurrentOrerData{ get; set; }
        public ConcurrentDictionary<int, int> HospitalPeopleByDays { get; set; }
        public ConcurrentDictionary<DateTime, int> QuarantinPeopleByDate { get; set; }
        

    }
}
