﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationCoronaTrackerYG
{
    [Serializable]
    public class LabStats
    {
        public int Kits { get; set; }
        public int TestFinished { get; set; }
        public int TestRejected { get; set; }
        public int TestInProcess { get; set; }
    }
}
