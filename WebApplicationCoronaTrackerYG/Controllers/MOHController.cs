﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoronaTrackerBL.LaboratoriesManager;
using CoronaTrackerBL.Laboratory;
using CoronaTrackerBL.LogisticsManager;
using CoronaTrackerBL.Manager;
using CoronaTrackerBL.Police;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplicationCoronaTrackerYG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MOHController : ControllerBase
    {
        private Police _police;
        private LogisticsManager _logisticsManger;
        private QuarantineManager _quarantineManger;
        private LaboratoriesManager _laboratoriesManger;
        private InfectedManager _infectedManager;
        public MOHController()
        {
            _police = Police.GetInstance();
            _logisticsManger = LogisticsManager.GetInstance();
            _quarantineManger = QuarantineManager.GetInstance();
            _laboratoriesManger = LaboratoriesManager.GetInstance();
            _infectedManager = InfectedManager.GetInstance();
        }

        [HttpPost]
        [Route("reportSickCall")]
        public void ReportSickCall(long id)
        {
            _police.ReportCompromize(id, DateTime.Today);
        }

        [HttpPost]
        [Route("reportDamagedStock")]
        public void ReportDamagedStock(int amount)
        {
            _logisticsManger.RemoveTestKits(amount);
        }

        [HttpPost]
        [Route("startQuarantine")]
        public void StartQuarantine()
        {
            _quarantineManger.StartQuarantine();
        }

        [HttpPost]
        [Route("stopQuarantine")]
        public void StopQuarantine()
        {
            _quarantineManger.StopQuarantine();
        }

        [HttpGet]
        [Route("statistics")]
        public Stats GetStatistics()
        {
            Stats statistics = new Stats();
            statistics.HospitalPeopleByDays = _infectedManager.AllInfectedGroupByDays;
            statistics.QuarantinPeopleByDate = _quarantineManger._personQuarantineByReleaseDate;
            statistics.InventoryTotalKits = _logisticsManger.KitsAmount;
            statistics.WithdrawKits = _logisticsManger.KitsDistrbuted;
            if (_logisticsManger.HasOrdered())
            {
                statistics.CurrentOrerData = _logisticsManger.CurrentOrder;
            }
            var labs = _laboratoriesManger.Laboratories;
            foreach (Laboratory lab in labs)
            {
                LabStats labstats = new LabStats();
                statistics.TotalTestInProcess += lab.TestsCurrentInLab;
                labstats.TestInProcess = lab.TestsCurrentInLab;
                statistics.TotalTestRejected += lab.TestsRejectedInLab;
                labstats.TestRejected = lab.TestsRejectedInLab;
                statistics.TotalTestFinished += lab.TestsFinishedInLab;
                labstats.TestFinished = lab.TestsFinishedInLab;
                statistics.TotalKits += lab.KitsInStock;
                labstats.Kits = lab.KitsInStock;
                statistics.Laboratories.Add(labstats);
            }
            return statistics;
            
        }










    }
}