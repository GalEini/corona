﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoronaTrackerBL.Police;
using Microsoft.AspNetCore.Mvc;

namespace WebApplicationCoronaTrackerYG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PoliceAPIController : ControllerBase
    {
        Police police = Police.GetInstance();
        [HttpPost]
        [Route("reportUnauthorizedWalker")]
        public void ReportUnauthorizedWalker(long personID)
        {
            police.ForceQuarantine(personID);
        }
        [HttpPost]
        [Route("reportReturnigFromAbroad")]
        public void ReportReturnigFromAbroad(long personID, DateTime arrivalDate)
        {
            police.ReportCompromize(personID, arrivalDate);
        }
        [HttpGet]
        [Route("getOpenSearchesForCompromised")]
        public int GetOpenSearchesForCompromised()
        {
            return police.CompromisedDictionary.Count;
        }
        [HttpPost]
        [Route("inputResultsForASearch")]
        public void InputResultsForASearch(long personID, List<long> personsCompromised)
        {
            if (police.CompromisedDictionary[personID].Item1.IsCompleted)
            {
                police.IDToCompromised.Add(personID, personsCompromised);
                police.CompromisedDictionary[personID].Item2.Cancel();
            }
            else
                police.InvokeCompromised(personsCompromised);
        }
    }
}
