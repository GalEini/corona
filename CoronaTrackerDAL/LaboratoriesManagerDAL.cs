﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace CoronaTrackerDAL
{
    public class LaboratoriesManagerDAL
    {
        private static readonly string conStr =
            @"Data Source=DESKTOP-GAL-EIN;Initial Catalog=CoronaTrackerGYDB;"
            + "Integrated Security=true";
        public static List<(int LaboratoryID, int Laboratorians, int addressLat, int addressLongt, int kits, int test)> GetLaboratories()
        {
            int id = 0, lat = 0, longt = 0, Laboratorians = 0, kits = 0, test = 0;
            List<(int LaboratoryID, int Laboratorians, int addressLat, int addressLongt, int kits, int test) > labsList =
                new List<(int LaboratoryID, int Laboratorians, int addressLat, int addressLongt, int kits, int test)> ();
            try
            {

                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand($"SELECT *" +
                        $" FROM Laboratory ", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                    {
                        id = Int32.Parse(dataReader["LaboratoryID"].ToString());
                        lat = Int32.Parse(dataReader["Addresslatitude"].ToString());
                        longt = Int32.Parse(dataReader["Addresslang"].ToString());
                        Laboratorians = Int32.Parse(dataReader["Laboratorians"].ToString());
                        kits = Int32.Parse(dataReader["KitsStock"].ToString());
                        test = Int32.Parse(dataReader["CurrentTestInLab"].ToString());
                        labsList.Add((id, Laboratorians, lat, longt, kits, test));
                    }
                }
                return labsList;
            }
            catch (Exception)
            {

                Console.WriteLine("DB problem");
                return null;
            }
        }
        public static (List<(long personID, DateTime reportDate, int EventID)> unauthorizedList,
            List<(long personID, DateTime reportDate, int EventID)> foundCompromisedList, List<(long personID, int EventID)> quarantineEndedList) InitlabsManager()
        {
            List<(long personID, DateTime reportDate, int EventID)> unauthorizedList = new List<(long personID, DateTime reportDate, int EventID)>();
            List<(long personID, DateTime reportDate, int EventID)> foundCompromisedList = new List<(long personID, DateTime reportDate, int EventID)>();
            List<(long personID, int EventID)> quarantineEndedList = new List<(long personID, int EventID)>();
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();//left outer join
                    SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM ActionLogUnauthorizedWalker LEFT OUTER JOIN" +
                        " (SELECT * FROM Reaction" +
                        " WHERE Reaction.ReactionType = 6) rc  ON ActionLogUnauthorizedWalker.EventID = rc.EventID", con);
                    dataAdapter.Fill(ds);
                    dataAdapter = new SqlDataAdapter("SELECT * FROM ActionLogFoundCompromised LEFT OUTER JOIN" +
                        " (SELECT * FROM Reaction" +
                        " WHERE Reaction.ReactionType = 6) rc  ON ActionLogFoundCompromised.EventID = rc.EventID", con);
                    dataAdapter.Fill(ds);
                    dataAdapter = new SqlDataAdapter("SELECT * FROM ActionLogQuarantinEnded LEFT OUTER JOIN" +
                        " (SELECT * FROM Reaction" +
                        " WHERE Reaction.ReactionType = 6) rc  ON ActionLogQuarantinEnded.EventID = rc.EventID", con);
                    dataAdapter.Fill(ds);
                }
                var unauthorizedWalkerTable = ds.Tables[0];
                var foundCompromisedTable = ds.Tables[1];
                var quarantineEndedListTable = ds.Tables[2];
                for (int i = 0; i < unauthorizedWalkerTable.Rows.Count; i++)
                {
                    if (unauthorizedWalkerTable.Rows[i]["ReactionID"] == null)
                    {
                        unauthorizedList.Add((long.Parse(unauthorizedWalkerTable.Rows[i]["PersonID"].ToString()), DateTime.Parse(unauthorizedWalkerTable.Rows[i]["ReportDate"].ToString())
                            , Int32.Parse(unauthorizedWalkerTable.Rows[i]["EventID"].ToString())));
                    }
                }
                for (int i = 0; i < foundCompromisedTable.Rows.Count; i++)
                {
                    if (foundCompromisedTable.Rows[i]["ReactionID"] == null)
                    {
                        foundCompromisedList.Add((long.Parse(foundCompromisedTable.Rows[i]["PersonID"].ToString()), DateTime.Parse(foundCompromisedTable.Rows[i]["DateComp"].ToString())
                            , Int32.Parse(unauthorizedWalkerTable.Rows[i]["EventID"].ToString())));
                    }
                }
                for (int i = 0; i < quarantineEndedListTable.Rows.Count; i++)
                {
                    if (quarantineEndedListTable.Rows[i]["ReactionID"] == null)
                    {
                        quarantineEndedList.Add((long.Parse(quarantineEndedListTable.Rows[i]["PersonID"].ToString())
                            , Int32.Parse(unauthorizedWalkerTable.Rows[i]["EventID"].ToString())));
                    }
                }
                return (unauthorizedList, foundCompromisedList, quarantineEndedList);
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
                return (null, null, null);
            }
        }
    }
}
