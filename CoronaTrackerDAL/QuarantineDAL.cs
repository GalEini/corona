﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace CoronaTrackerDAL
{
    public class QuarantineDAL
    {
        private static readonly string connectionString =
             @"Data Source=desktop-yael\sqlexpress;Initial Catalog=CoronaTrackerGYDB;"
             + "Integrated Security=true";

        public static void AddPersonToDB(long personID, DateTime effectiveDate)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                String query = "INSERT INTO Quarantines (personID, Until) VALUES (@personID,@untilDate)";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                    command.Parameters.AddWithValue("@personID", personID);
                    command.Parameters.AddWithValue("@untilDate", effectiveDate);
                    connection.Open();
                    
                        int result = command.ExecuteNonQuery();
                        if (result < 0)
                            Console.WriteLine("Error inserting data into Database!");
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Error from QuarantineDAL: "+e.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error from QuarantineDAL: " + ex.Message);
                    }

                }
                connection.Dispose();
            }
        }

        public static void ReleasePersonFromQuaDB(long personID)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    String query = "UPDATE Quarantines SET status = 0 WHERE personID = @personID AND status = 1";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@personID", personID);
                        //command.Parameters.AddWithValue("@status", 0);
                        //command.Parameters.AddWithValue("@currentStatus", 1);
                        connection.Open();
                        int result = command.ExecuteNonQuery();
                        if (result < 0)
                            Console.WriteLine("Error inserting data into Database!");
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Error from QuarantineDAL: " + e.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error from QuarantineDAL: " + ex.Message);
                }
            }
        }

        public static ConcurrentDictionary<long, DateTime> PopulateSoonToRelease()
        {
            try
            {
                ConcurrentDictionary<long, DateTime> soonToRelease = new ConcurrentDictionary<long, DateTime>();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("Select * from Quarantines where until <= @tomorrow AND status = 1", con);
                    command.Parameters.AddWithValue("@tomorrow", DateTime.UtcNow.AddDays(1).Date);
                    SqlDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        soonToRelease.TryAdd(int.Parse(dataReader[1].ToString()), (DateTime)dataReader[2]);
                    }

                }
                return soonToRelease;
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error from QuarantineDAL: " + e.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error from QuarantineDAL: " + ex.Message);
            }
            return null;
        }

        public static (List<(long personID, DateTime reportDate, int eventID)> AddtoQuarantineList, List<(long personID, bool result,int eventID)> ReleaseFromQuarnList) InitQuarantinManger()
        {
            List<(long personID, DateTime reportDate, int EventID)> AddtoQuarantineList = new List<(long personID, DateTime reportDate,int EventID)> ();
            List<(long personID, bool result, int EventID)> ReleaseFromQuarnList = new List<(long personID, bool result, int EventID)>();
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM(SELECT * FROM ActionLogUnauthorizedWalker UNION"
                    +"SELECT * FROM ActionLogFoundCompromised ) UnionTable LEFT OUTER JOIN(SELECT * FROM Reaction  WHERE Reaction.ReactionType = 1) rc  ON UnionTable.EventID = rc.EventID ", con);
                    dataAdapter.Fill(ds);
                    dataAdapter = new SqlDataAdapter("SELECT * FROM ActionLogInfectionResult LEFT OUTER JOIN(SELECT * FROM Reaction  WHERE Reaction.ReactionType = 2) rc  ON ActionLogInfectionResult.EventID = rc.EventID ", con);
                    dataAdapter.Fill(ds);
                }
                var unauthWalkerAndFoundCompTable = ds.Tables[0];
                var testResultTable = ds.Tables[1];
                for (int i = 0; i < unauthWalkerAndFoundCompTable.Rows.Count; i++)
                {
                    if (unauthWalkerAndFoundCompTable.Rows[i]["ReactionID"] == null)
                    {
                        var row = unauthWalkerAndFoundCompTable.Rows[i];
                        AddtoQuarantineList.Add((long.Parse(row["PersonID"].ToString()), DateTime.Parse(row["ReportDate"].ToString())
                                                    , Int32.Parse(row["EventID"].ToString())));
                    }
                }
                for (int i = 0; i < testResultTable.Rows.Count; i++)
                {
                    if (testResultTable.Rows[i]["EventLog"] == null)
                    {
                        var row = testResultTable.Rows[i];
                        ReleaseFromQuarnList.Add((long.Parse(row["PersonID"].ToString()), bool.Parse(row["Result"].ToString())
                           , Int32.Parse(row["EventID"].ToString())));
                    }
                }
                return (AddtoQuarantineList, ReleaseFromQuarnList);
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
                if (AddtoQuarantineList?.Count > 0)
                {
                    if (ReleaseFromQuarnList?.Count > 0)
                    {
                        return (AddtoQuarantineList, ReleaseFromQuarnList);
                    }
                    return (AddtoQuarantineList, null);
                }
                else if (ReleaseFromQuarnList?.Count > 0)
                {
                    return (null, ReleaseFromQuarnList);
                }
                return (null, null);
            }
        }

        public static DateTime? GetDaysOfPerson(long personID)
        {
            DateTime? resultDate = null;
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand($"Select until from Quarantines where personID={personID} AND status = 1", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                    {
                        resultDate = (DateTime)dataReader[0];
                    }
                }
                return resultDate;
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error from QuarantineDAL: " + e.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error from QuarantineDAL: " + ex.Message);
            }
            return null;
        }
    }
}
