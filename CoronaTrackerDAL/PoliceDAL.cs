﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace CoronaTrackerDAL
{
    public class PoliceDAL
    {
        private static readonly string conStr =
             @"Data Source=DESKTOP-GAL-EIN;Initial Catalog=CoronaTrackerGYDB;"
             + "Integrated Security=true";
        public static void OpenCriminalRecord(long personID)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO CriminalRecord (PersonID) " +
                        $"VALUES ({personID});", con);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
            }
        }
        public static List<long> ListOfIDs(int amountIDs)
        {
            List<long> listID = new List<long>(amountIDs);
            DataSet ds = new DataSet();
            try
            {
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter($"SELECT TOP {amountIDs} * " +
                    $"FROM People " +
                    $"ORDER BY RAND();",con);
                dataAdapter.Fill(ds);
            }
            var table = ds.Tables[9];
            for (int i = 0; i < table.Rows.Count; i++)
            {
                listID.Add(long.Parse(table.Rows[i]["PersonID"].ToString()));
            }
            return listID;

            }
            catch (Exception)
            {

                Console.WriteLine("DB problem");
                return null;
            }
        }
        public static (List<(long personID, DateTime reportDate, int EventID)> unauthorizedList, List<(long personID, bool result, int EventID)> infectionList) InitPolice()
        {
            List<(long personID, DateTime reportDate, int EventID)> unauthorizedList = new List<(long personID, DateTime reportDate, int EventID)>();
            List<(long personID, bool result, int EventID)> infectionList = new List<(long personID, bool result, int EventID)>();
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();//left outer join
                    SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM ActionLogUnauthorizedWalker LEFT OUTER JOIN" +
                        " (SELECT * FROM Reaction" +
                        " WHERE Reaction.ReactionType = 4) rc  ON ActionLogUnauthorizedWalker.EventID = rc.EventID", con);
                    dataAdapter.Fill(ds);
                    dataAdapter = new SqlDataAdapter("SELECT * FROM ActionLogInfectionResult LEFT OUTER JOIN" +
                        " (SELECT * FROM Reaction" +
                        " WHERE Reaction.ReactionType = 5) rc  ON ActionLogUnauthorizedWalker.EventID = rc.EventID", con);
                    dataAdapter.Fill(ds);
                }
                var unauthorizedWalkerTable = ds.Tables[0];
                var infectionResultTable = ds.Tables[1];
                for (int i = 0; i < unauthorizedWalkerTable.Rows.Count; i++)
                {
                    if (unauthorizedWalkerTable.Rows[i]["ReactionID"] == null)
                    {
                        unauthorizedList.Add((long.Parse(unauthorizedWalkerTable.Rows[i]["PersonID"].ToString()), DateTime.Parse(unauthorizedWalkerTable.Rows[i]["ReportDate"].ToString())
                            , Int32.Parse(unauthorizedWalkerTable.Rows[i]["EventID"].ToString())));
                    }
                }
                for (int i = 0; i < infectionResultTable.Rows.Count; i++)
                {
                    if (infectionResultTable.Rows[i]["ReactionID"] == null)
                    {
                        infectionList.Add((long.Parse(infectionResultTable.Rows[i]["PersonID"].ToString()), bool.Parse(infectionResultTable.Rows[i]["Result"].ToString())
                            , Int32.Parse(unauthorizedWalkerTable.Rows[i]["EventID"].ToString())));
                    }
                }
                return (unauthorizedList, infectionList);
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
                return (null, null);
            }
        }
    }
}
