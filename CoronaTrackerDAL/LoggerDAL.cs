﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace CoronaTrackerDAL
{
    public class LoggerDAL
    {
        private static readonly string conStr =
             @"Data Source=DESKTOP-GAL-EIN;Initial Catalog=CoronaTrackerGYDB;"
             + "Integrated Security=true";
        public static int LogFoundCompromisedEvent(long personID, DateTime dateComp)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO ActionLogFoundCompromised  (PersonID, DateComp) " +
                        "OUTPUT INSERTED.EventID" +
                        $"VALUES ({personID}, {dateComp});", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                        return Int32.Parse(dataReader[0].ToString());
                    else
                        return 0;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
                return 0;
            }
        }
        public static int LogQuarantineEndedEvent(long personID)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO ActionLogQuarantinEnded (PersonID) " +
                        "OUTPUT INSERTED.EventID" +
                        $"VALUES ({personID});", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                        return Int32.Parse(dataReader[0].ToString());
                    else
                        return 0;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
                return 0;
            }
        }
        public static int LogInfectionResultEvent(long personID, bool result)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO ActionLogInfectionResult (PersonID, Result) " +
                        "OUTPUT INSERTED.EventID" +
                        $"VALUES ({personID}, {result});", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                        return Int32.Parse(dataReader[0].ToString());
                    else
                        return 0;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
                return 0;
            }
        }
        public static int LogUnauthorizedWalkerEvent(long personID, DateTime reportDate)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO ActionLogUnauthorizedWalker (PersonID, ReportDate) " +
                        "OUTPUT INSERTED.EventID" +
                        $"VALUES ({personID}, {reportDate});", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                        return Int32.Parse(dataReader[0].ToString());
                    else
                        return 0;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
                return 0;
            }
        }
        public static void LogReaction(int eventId, int eveמtLog)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO ActionLogReaction (EventId, EventLog) " +
                        $"VALUES ({eventId}, {eveמtLog});", con);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
            }
        }
    }
}
