﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace CoronaTrackerDAL
{
    public class StatsDAL
    {
        private static readonly string connectionString =
             @"Data Source=desktop-yael\sqlexpress;Initial Catalog=CoronaTrackerYaelGal;"
             + "Integrated Security=true";

        public static ConcurrentDictionary<DateTime, int> GetAllQuarGroupByDate()
        {
            try
            {
                ConcurrentDictionary<DateTime, int> quarntines = new ConcurrentDictionary<DateTime, int>();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("select until, count(*) from Quarantines where Status = 1 Group by Until", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        quarntines.TryAdd((DateTime)dataReader[0], int.Parse(dataReader[1].ToString()));
                    }

                }
                return quarntines;
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error from QuarantineDAL: " + e.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error from QuarantineDAL: " + ex.Message);
            }
            return null;
        }
        public static ConcurrentDictionary<int, int> GetAllInfecGroupByDays()
        {
            try
            {
                ConcurrentDictionary<int, int> infected = new ConcurrentDictionary<int, int>();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("select DaysSinceDiagnose, count(*) from Infecteds where Status = 1 Group by DaysSinceDiagnose", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        infected.TryAdd(int.Parse(dataReader[0].ToString()), int.Parse(dataReader[1].ToString()));
                    }

                }
                return infected;
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error from QuarantineDAL: " + e.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error from QuarantineDAL: " + ex.Message);
            }
            return null;
        }
    }
}
