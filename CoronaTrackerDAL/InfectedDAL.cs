﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace CoronaTrackerDAL
{
    public class InfectedDAL
    {
        private static readonly string connectionString =
             @"Data Source=desktop-yael\sqlexpress;Initial Catalog=CoronaTrackerGYDB;"
             + "Integrated Security=true";
        public static void AddPersonToHospital(long personID)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    String query = "INSERT INTO Infecteds (personID, DaysSinceDiagnose) VALUES (@personID,@DaysSinceDiagnose)";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@personID", personID);
                        command.Parameters.AddWithValue("@DaysSinceDiagnose", 1);
                        connection.Open();
                        int result = command.ExecuteNonQuery();
                        if (result < 0)
                            Console.WriteLine("Error inserting data into Database!");
                    }
                    connection.Dispose();
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Error from InfectedDAL: " + e.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error from InfectedDAL: " + ex.Message);
                }
            }
        }
        public static void IncrementSickDays()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    String query = "UPDATE Infecteds SET DaysSinceDiagnose = DaysSinceDiagnose + 1 WHERE status = 1";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        connection.Open();
                        int result = command.ExecuteNonQuery();
                        if (result < 0)
                            Console.WriteLine("Error inserting data into Database!");
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Error from InfectedDAL: " + e.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error from InfectedDAL: " + ex.Message);
                }
            }
        }

        public static void ReleasePersonFromHospDB(long personID)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    String query = "UPDATE Infecteds SET status = 0 WHERE personID = @personID AND status = 1";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@personID", personID);
                        connection.Open();
                        int result = command.ExecuteNonQuery();
                        if (result < 0)
                            Console.WriteLine("Error inserting data into Database!");
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Error from InfectedDAL: " + e.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error from InfectedDAL: " + ex.Message);
                }
            }
        }

        public static Dictionary<long, int> PopulateHospitalPatients()
        {
            try
            {
                Dictionary<long, int> patients = new Dictionary<long, int>();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("Select * from Infecteds where status = 1", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        patients.Add(int.Parse(dataReader[1].ToString()), int.Parse(dataReader[2].ToString()));
                    }

                }
                return patients;
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error from InfectedDAL: " + e.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error from InfectedDAL: " + ex.Message);
            }
            return null;
        }

        public static List<(long personID, bool result, int eventID)> InitInfectedManger()
        {
            List<(long personID, bool result, int EventID)> ReleaseFromQuarnList = new List<(long personID, bool result, int EventID)>();
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM ActionLogInfectionResult LEFT OUTER JOIN(SELECT * FROM Reaction  WHERE Reaction.ReactionType = 3) rc  ON ActionLogInfectionResult.EventID = rc.EventID ", con);
                    dataAdapter.Fill(ds);
                }
                var testResultTable = ds.Tables[0];
                for (int i = 0; i < testResultTable.Rows.Count; i++)
                {
                    if (testResultTable.Rows[i]["EventLog"] == null)
                    {
                        var row = testResultTable.Rows[i];
                        ReleaseFromQuarnList.Add((long.Parse(row["PersonID"].ToString()), bool.Parse(row["Result"].ToString())
                           , Int32.Parse(row["EventID"].ToString())));
                    }
                }
                return (ReleaseFromQuarnList);
            }
            catch (Exception)
            {
                Console.WriteLine("DB problem");
                return (null);
            }
        }

    }
}
