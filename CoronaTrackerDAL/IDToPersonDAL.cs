﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;

namespace CoronaTrackerDAL
{
    public class IDToPersonDAL
    {
        private static readonly string conStr =
             @"Data Source=DESKTOP-GAL-EIN;Initial Catalog=CoronaTrackerGYDB;"
             + "Integrated Security=true";
        public static (long personID, string name, int addressLat, int addressLongt) GetPerson(long PersonID)
        {
            string name=null;
            long id=0;
            int lat=0, longt=0;
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand($"SELECT * FROM People WHERE People.PersonID = {PersonID}", con);
                    SqlDataReader dataReader = command.ExecuteReader();
                    if (dataReader.Read())
                    {
                        name = dataReader["name"].ToString();
                        id = long.Parse(dataReader["personid"].ToString());
                        lat = Int32.Parse(dataReader["addresslatitude"].ToString());
                        longt = Int32.Parse(dataReader["addresslang"].ToString());
                    }
                }
                return (id, name, lat, longt);
            }
            catch (Exception)
            {
                throw new Exception();
            }
            
        }       
    }
}
